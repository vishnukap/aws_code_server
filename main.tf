terraform {
  required_providers {
    ct = {
      source  = "poseidon/ct"
      version = "0.13.0"
    }
  }

  backend "http" {
  }
}

provider "ct" {
  # Configuration options
}

provider "aws" {
  region = var.region
}

resource "random_string" "app_name" {
  length  = 5
  upper   = false
  special = false
}

locals {
  domain   = "${var.subdomain}.${var.root_domain}"
  app_name = "${var.app_name}-${random_string.app_name.result}"
}

data "ct_config" "this" {
  content = templatefile("config.bu", {
    domain   = local.domain
    app_port = var.app_port
    coder_password = var.coder_password
  })
  strict       = true
  pretty_print = false
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${local.app_name}-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["${var.region}a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = false

  tags = {
    Terraform = "true"
  }
}

module "coder" {
  source               = "git::https://gitlab.com/vishnukap/terraform_coreos_infra.git"
  cloudflare_api_token = var.cloudflare_api_token
  instance_size        = var.instance_size
  region               = var.region
  ami                  = var.ami
  root_domain          = var.root_domain
  subdomain            = var.subdomain
  app_name             = var.app_name
  user_data            = data.ct_config.this.rendered
  subnet_id            = module.vpc.public_subnets[0]
  app_port             = var.app_port
}

output "cloudflare_domain" {
  value = "https://${local.domain}:8443"
}