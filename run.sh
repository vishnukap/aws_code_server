#!/bin/bash

# path to butane config
BUTANE_CONFIG="./config.bu"

podman run --interactive --rm quay.io/coreos/butane:release \
       --pretty --strict < ${BUTANE_CONFIG} > transpiled_config.ign

IGNITION_CONFIG="./transpiled_config.ign"
IMAGE="/var/home/roguecomp/.local/share/libvirt/images/fedora-coreos-40.20240504.3.0-qemu.x86_64.qcow2"

# for x86/aarch64:
IGNITION_DEVICE_ARG="-fw_cfg name=opt/com.coreos/config,file=${IGNITION_CONFIG}"

qemu-kvm -m 2048 -cpu host -nographic -snapshot \
  -drive if=virtio,file=${IMAGE} ${IGNITION_DEVICE_ARG} \
  -nic user,model=virtio,hostfwd=tcp::2222-:22,hostfwd=tcp::8443-:8443
