# Code Server in AWS Subnet
### Source
Find the source [here](https://gitlab.com/vishnukap/aws_code_server)

## Description
Deploy your own code server on AWS. Useful to run web IDE directly from inside a VPC to access private resources.

### The module
* generates SSL certificate using Let's Encrypt and Caddy
* Points CloudFlare domain to the code server
* Deploys code server into an AWS subnet ID. This is useful for accessing private AWS resources

### Example Usage

``` terraform
resource "random_string" "subdomain_suffix" {
  length  = 4
  special = false
  upper   = false
}

locals {
  subdomain   = "code-${random_string.subdomain_suffix.result}"
  root_domain = "<root-domain>" # eg: example.com
  region      = "<aws-region>"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "code-server"
  cidr = "10.0.0.0/16"

  azs             = ["${local.region}a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = false

  tags = {
    Terraform = "true"
  }
}

resource "random_string" "password" {
  length  = 32
  special = false
}

module "code_server" {
  source                       = "git@gitlab.com:vishnukap/aws_code_server.git"
  region                       = local.region
  root_domain                  = local.root_domain
  subdomain                    = local.subdomain
  instance_type                = "t3.small"
  subnet_id                    = module.vpc.public_subnets[0]
  code_server_initial_password = random_string.password.result
  cloudflare_api_token         = "<cloudflare-api-token>"

}

output "code_server_uri" {
  value = "https://${local.subdomain}.${local.root_domain}"
}

output "code_server_initial_password" {
  value = random_string.password.result
}
```